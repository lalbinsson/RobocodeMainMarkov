package Grupp03;


	import java.awt.geom.Point2D;

import robocode.Robot;
import robocode.util.Utils;
	import sun.reflect.generics.reflectiveObjects.NotImplementedException;

	/**
	 * A simple class handling aiming and firing. It simply shoots at the closest target 
	 * based on the last known positions of all enemies.
	 */
	public class TargetingSystem {
		
		// ETSA02 Lab2: Add attributes according to the provided UML class diagram.
		private EnemyTracker enemyTracker;
		private	Robot05 robot;
		/**
		 * Construct a simple object to handle head-on targeting. It simply shoots at the closest target 
		 * based on the last known positions of all enemies.
		 * @param enemyTracker the object managing enemies.
		 * @param robot the robot we are currently working on
		 */
		public TargetingSystem(EnemyTracker enemyTracker, Robot05 robot) {
			this.robot=robot;
			this.enemyTracker=enemyTracker;
			
			// ETSA02 Lab2: Implement this constructor to initiate the attributes.
			
		}
		
		/**
		 * To be called every turn. Find the closest target and shoot at it.
		 */
		public void update() {
			// ETSA02 Lab2: Implement this by adapting the parts under AIMING AND SHOOTING.
			// AIMING AND SHOOTING
			Point2D.Double robotPosition = new Point2D.Double( robot.getX(), robot.getY());
						// find the closest enemy and store its position
						double smallestDistanceSq = Double.POSITIVE_INFINITY;
						Point2D.Double pointToTurnTo = null;
						Point2D.Double[]enemyPositions =enemyTracker.getEnemyPositions();
						for (int i = 0; i < enemyTracker.getEnemyCount(); i++) {
							Point2D.Double enemyPosition = enemyPositions[i];
							double d = enemyPosition.distanceSq(robotPosition);
							if (d < smallestDistanceSq) {
								smallestDistanceSq = d;
								pointToTurnTo = enemyPosition;
							}
						}
						//compute how much our robot's gun has to turn
						double angleToTurn = 0;
						if (pointToTurnTo != null) {
							double dx = pointToTurnTo.x - robotPosition.x;
							double dy = pointToTurnTo.y - robotPosition.y;
							angleToTurn = Math.toDegrees(Math.atan2(dx, dy)) - robot.getGunHeading();
						}

						// fire when the gun has finished turning
						if (robot.getGunTurnRemaining() == 0) {
							robot.setFire(1);
						}
						// turn the gun
						robot.setTurnGunRight(Utils.normalRelativeAngleDegrees(angleToTurn));
						
		}
}
