package Grupp03;



import robocode.*;
import robocode.util.Utils;

import java.awt.geom.Point2D;
import java.awt.Color;
import java.awt.Point;
import java.io.IOException;





public class Robot09 extends TeamRobot {

	boolean peek; // Don't turn if there's a robot there
	double moveAmount; // How much to move
	// boolean right; // Denna boolean är true när roboten åker åt höger och
	// false
	// när den kör åt vänster
	private boolean deffensive = true;
	boolean b = true;
	boolean wall;
	private boolean right = true;
	// private Robot05 robot;

	private double wallMargin = 4;
	private int teammate = 0;
	
	private int nr=0;

	private MovementSystem_def move1 = new MovementSystem_def(this);
	private MovementSystem_off move2 = new MovementSystem_off(this);
	
	public void run() {

		moveAmount = Math.max(getBattleFieldWidth()-1, getBattleFieldHeight()-1);
		// Initialize peek to false
		peek = false;

		turnLeft(getHeading() % 90);
		ahead(moveAmount);
		peek = true;
		turnGunRight(90);
		turnRight(90);

	
		
		while (true) {
		
			
			if (teammate >= 2) {
				deffensive = false;
				nr++;
			}
			if(nr==1){
				
				
			}
			
			if (deffensive) {
				move1.update(right); //moveAmount, 
			} else {
				move2.update();
			}

		}
	}

public void onScannedRobot(ScannedRobotEvent e) {

		if (isTeammate(e.getName())) {
			return;
		} else if (!deffensive) {
			fire(3);

		}
		// Calculate enemy bearing
		double enemyBearing = this.getHeading() + e.getBearing();
		// Calculate enemy's position
		double enemyX = getX() + e.getDistance() * Math.sin(Math.toRadians(enemyBearing));
		double enemyY = getY() + e.getDistance() * Math.cos(Math.toRadians(enemyBearing));

		try {
			// Send enemy position to teammates
			Point point = new Point();
			point.setLocation(enemyX, enemyY);
			broadcastMessage(point);
		} catch (IOException ex) {
			out.println("Unable to send order: ");
			ex.printStackTrace(out);
		}

	}

	// Don't get too close to the walls
public void onHitByBullet(HitByBulletEvent e) {
	// Replace the next line with any behavior you would like
	if (-30<e.getBearing() && e.getBearing() <30 ) {
		turnRight(180);
		turnGunRight(180);
		ahead(moveAmount);
		right = !right;
		

	}
}

	public void onRobotDeath(RobotDeathEvent event) {
		if (isTeammate(event.getName())) {
			teammate++;
		} 
	}

	/**
	 * Skriv en metod som kollar när roboten åker in i en vägg
	 */
public void onHitRobot(HitRobotEvent e) {
		
		if (e.getBearing() > -90 && e.getBearing() < 90) {
			turnRight(180);
			turnGunRight(180);
			ahead(moveAmount);
			right =!right;
		}
		else {
			ahead(100);
		}
	}



	

}
