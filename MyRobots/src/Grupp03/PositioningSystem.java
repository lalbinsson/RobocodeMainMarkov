package Grupp03;


	
	import java.awt.geom.Point2D;

	

	/**
	 * A class providing support for calculation related to the battle field.
	 */
	public class PositioningSystem {

		public enum Wall {
			TOP, LEFT, RIGHT, BOTTOM
		};

		private double width;
		private double height;
		private Robot05 robot;
		

		// ETSA02 Lab2: Add attributes according to the provided UML class diagram.

		/**
		 * Construct an object to help with battle field related calculations.
		 * 
		 * @param width
		 *            the width of the battle field.
		 * @param height
		 *            the height of the battle field.
		 */
		public PositioningSystem(double width, double height, Robot05 robot) {
			// ETSA02 Lab2: Implement this constructor to initiate the attributes.
			this.width = width;
			this.height = height;
			
			// throw new NotImplementedException();
		}

		/**
		 * Check whether a certain point is close to one of the walls. If so returns
		 * that wall.
		 * 
		 * @param point
		 *            the point to check.
		 * @param nearWallDistance
		 *            the distance from the wall.
		 * @return if the distance from the point to one of the walls is less than
		 *         nearWallDistance, return that wall. Otherwise, return null.
		 */
		public Wall checkCloseToWall(Point2D.Double point, double nearWallDistance) {
			// ETSA02 Lab2: Implement this method using the corresponding unit tests
			// as a guide.
			double min = nearWallDistance;
			Wall wall = null;
			

			if (point.getX() < min) {

				min = point.getX();
				wall = Wall.LEFT;

			}
			if ((width - point.getX()) < min) {

				min = width - point.getX();
				wall = Wall.RIGHT;

			}
			if (point.getY() < min) {
				min = point.getY();
				wall = Wall.BOTTOM;

			}
			if ((height - point.getY()) < min) {

				wall = Wall.TOP;
			}

			return wall;

		}

		/**
		 * Compute the projection of a point on a wall.
		 * 
		 * @param point
		 *            the point to compute.
		 * @param wall
		 *            the wall to compute.
		 * @return the projection of the point on the wall.
		 */
		public Point2D.Double getProjectionOnWall(Point2D.Double point, Wall wall) {
			// ETSA02 Lab2: Implement this method using the corresponding unit tests
			// as a guide.
			Point2D.Double p = null;
			switch (wall) {
			case TOP:
				p = new Point2D.Double(point.getX(), height);
				break;
			case BOTTOM:
				p = new Point2D.Double(point.getX(), 0);
				break;
			case RIGHT:
				p = new Point2D.Double(width, point.getY());
				break;
			case LEFT:
				p = new Point2D.Double(0, point.getY());
				break;

			}
			return p;

		}
		
		/*public void turning(double heading, Point2D.Double point, double nearWallDistance){
			Wall wall= checkCloseToWall(point,nearWallDistance );
			 if(wall==Wall.LEFT){
				if(heading>=90 || heading <=0){
					if(heading<=0){
						robot.turnLeft(Math.abs(heading)+45);
					}else{
						robot.turnRight(heading+45);
					}
				}else if( wall==Wall.RIGHT){
					if(heading<=-90 || heading >=0){
						if(heading>=90){
							
						}
					}
				}
				
			}*/
			
			
			
			
		}
		
		



