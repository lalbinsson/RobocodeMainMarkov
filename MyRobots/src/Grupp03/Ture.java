package Grupp03;

import robocode.*;
import java.awt.Color;

public class Ture extends Robot {
	
	boolean peek; // Don't turn if there's a robot there
	double moveAmount; // How much to move
	boolean right; //Denna boolean är true när roboten åker åt höger och false när den kör åt vänster
	
	public void run() {
		// Initialize moveAmount to the maximum possible for this battlefield.
		//Ändra moveAmount så att det anpassas efter längden till nästa vägg
		moveAmount = Math.max(getBattleFieldWidth(), getBattleFieldHeight());
		// Initialize peek to false
		peek = false;
		
		// turnLeft to face a wall.
		// getHeading() % 90 means the remainder of
		// getHeading() divided by 90.
		turnLeft(getHeading() % 90);
		ahead(moveAmount);
		// Turn the gun to turn right 90 degrees.
		peek = true;
		turnGunRight(90);
		turnRight(90);
		
		
		// Robot main loop
		while(true) {//while( X lagmedlemmar är kvar)
			// Replace the next 4 lines with any behavior you would like
			//Roboten rör sig längst väggarna 
			//Kontrollera vägg
			// Look before we turn when ahead() completes.
			peek = true;
			// Move up the wall9
			ahead(moveAmount);
			// Don't look now
			peek = false;
			// Turn to the next wall
			turnRight(90);
			//Kontrollera hur många spelare som är kvar på planen
			
		}
		//Om X antal lagmedlemmar är kvar på planen while(true)
		
		//Byt till andra fasen 
	}
	
	
	
	
	/**
	 * Skriv en metod som kollar när roboten åker in i en vägg
	 */
	
	
	/**
	 * onScannedRobot: What to do when you see another robot
	 */
	public void onScannedRobot(ScannedRobotEvent e) {
		// Replace the next line with any behavior you would like
		fire(1);
	}

	/**
	 * onHitByBullet: What to do when you're hit by a bullet
	 */
	/*public void onHitByBullet(HitByBulletEvent e) {
		// Replace the next line with any behavior you would like
		back(10);
	}
	*/
	
	/**
	 * onHitWall: What to do when you hit a wall
	 */
	/*public void onHitWall(HitWallEvent e) {
		// Replace the next line with any behavior you would like
		back(20);
	}	*/
	

}
