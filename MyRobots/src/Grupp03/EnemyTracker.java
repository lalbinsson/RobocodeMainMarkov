package Grupp03;


	import java.awt.geom.Point2D;

import robocode.ScannedRobotEvent;
	

	/**
	 * A class to help with the management of enemies.
	 */
	public class EnemyTracker {
		private Point2D.Double[] enemyPositions;
		private String[] enemyNames;
		private int enemyCount;
		private Robot05 robot;
		// ETSA02 Lab2: Add attributes according to the provided UML class diagram.
			
		/**
		 * Construct an object to help with the management of enemies.
		 * @param robot the robot we are working on.
		 */
		public EnemyTracker(Robot05 robot) {
			// ETSA02 Lab2: Implement this constructor to initiate the attributes.
			// Make room for 10 enemies when you create the arrays.
			enemyNames= new String[10];
			enemyPositions= new Point2D.Double[10];
			this.robot=robot;
		
		}
		
		/** 
		 * To be called when an enemy is scanned.
		 * @param e The ScannedRobotEvent received from the onScannedRobot method.
		 */ 
		public void addEnemy(ScannedRobotEvent e) {
			// ETSA02 Lab2: Add an enemy to the database. This behavior was previously in onScannedRobot()
			// You get the first line of code for free - uncomment it and add the rest
			
			Point2D.Double position = MathUtils.calcEnemyPosition(robot, e);
			String name=e.getName();
			int nr=findEnemyByName(name);
			
			if(nr<0){
				enemyNames[enemyCount]=name;
				 enemyPositions[enemyCount]=position;
				 enemyCount++;
			}else{
				enemyPositions[nr]=position;
			}
		}
		
		/**
		 * Stop tracking an enemy, i.e., when that enemy is dead.
		 * @param name the name of enemy to stop tracking.
		 */
		public void removeEnemy(String name) {
			// ETSA02 Lab2: Remove an enemy from the database. This behavior was previously in onRobotDeath()
			
			int index = findEnemyByName(name);
			if (index >= 0) {
				for (int i = index + 1; i < enemyCount; i++) {
					enemyNames[i - 1] = enemyNames[i];
					enemyPositions[i-1] = enemyPositions[i];
				}
				enemyCount--;
		
				}
			}
		
		

		
		/**
		 * Returns the last known positions of the enemies.
		 * @return An array of the last known enemy positions.
		 */
		public Point2D.Double[] getEnemyPositions() {
			// ETSA02 Lab2: You get this method for free. Uncomment the lines below.
			
			Point2D.Double[] positions = new Point2D.Double[enemyCount];
			for (int i = 0; i < enemyCount; i++) {
				positions[i] = enemyPositions[i];
			}
			return positions;		
			
		}
		
		/**
		 * @return the number of enemies currently being tracked.
		 */
		public int getEnemyCount() {
			// ETSA02 Lab2: Return the enemy count.
			return enemyCount;
		}
		
		private int findEnemyByName(String name) {
			// ETSA02 Lab2: Copy this method from BasicMeleeBot_AntiPattern.
			
			
				for (int i = 0; i < enemyCount; i++) {
					if (enemyNames[i].equals(name)){
						return i;
					}
				}
		
				return -1;
			
		}
	}

