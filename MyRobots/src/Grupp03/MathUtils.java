package Grupp03;


	import java.awt.geom.Point2D;

	import robocode.Robot;
	import robocode.ScannedRobotEvent;
	import sun.reflect.generics.reflectiveObjects.NotImplementedException;

	public class MathUtils {
		
		/**
		 * Computes the position of a scanned enemy.
		 * @param robot Our own robot.
		 * @param e The ScannedRobotEvent returned by Robocode.
		 * @return The position of the enemy robot in two dimensions.
		 */
		public static Point2D.Double calcEnemyPosition(Robot robot, ScannedRobotEvent event) {
			// ETSA02 Lab2: Implement this using basic trigonometry, i.e., bearing and heading.
			// This is described in http://mark.random-article.com/weber/java/robocode/lesson4.html
			// Note that this is already implemented in onScannedRobot() in BasicMeleeBot_AntiPattern
			
			double absBearing = event.getBearing() + robot.getHeading();
			double x = robot.getX() + event.getDistance() * Math.sin(Math.toRadians(absBearing));
			double y = robot.getY() + event.getDistance() * Math.cos(Math.toRadians(absBearing));
			
			return new Point2D.Double(x, y);
			//throw new NotImplementedException();
		}
		
		/** 
		 * Computes the angle in radians between the y-axis (equivalent to north in robocode's coordinate system) and the vector
		 * formed by the two points. (The vector points from p1 to p2).
		 * @param p1 The first point.
		 * @param p2 The second point.
		 * @return The angle between the two points, in radians.
		 */
		public static double calcAngle(Point2D.Double p1, Point2D.Double p2) {
			// ETSA02 Lab2: Copy this method from the BasicMeleeBot_AntiPattern class.
			
			
				double dx = p2.x - p1.x;
				double dy = p2.y - p1.y;
				return Math.atan2(dx, dy);
			
			//throw new NotImplementedException();
		}
		
		/**
		 * Computes the angle in degree between the y-axis (equivalent to north in robocode's coordinate system) and the vector
		 * formed by the two points. (The vector points from p1 to p2).
		 * @param p1 The first point.
		 * @param p2 The second point.
		 * @return The angle between the two points, in degrees.
		 */
		public static double calcAngleDegrees(Point2D.Double p1, Point2D.Double p2) {
			// ETSA02 Lab2: Call the method above and convert to degrees using Math.toDegrees()
			double angle =calcAngle(p1, p2);
			
			return Math.toDegrees(angle);
			
			//throw new NotImplementedException();
		}

}
