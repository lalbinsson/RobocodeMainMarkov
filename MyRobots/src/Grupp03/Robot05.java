package Grupp03;

import robocode.*;
import robocode.util.Utils;

import java.awt.geom.Point2D;
import java.awt.Color;
import java.awt.Point;
import java.io.IOException;





public class Robot05 extends TeamRobot {

	boolean peek; // Don't turn if there's a robot there
	double moveAmount; // How much to move
	// boolean right; // Denna boolean är true när roboten åker åt höger och
	// false
	// när den kör åt vänster
	private boolean deffensive = true;
	boolean b = true;
	boolean wall;
	private boolean right = true;
	// private Robot05 robot;

	private double wallMargin = 4;
	private int teammate = 0;
	private int enemy = 0;
	private int nr=0;

	private MovementSystem_def move1 = new MovementSystem_def(this);
	private MovementSystem_off move2 = new MovementSystem_off(this);
	
	public void run() {

		moveAmount = Math.max(getBattleFieldWidth()-1, getBattleFieldHeight()-1);
		// Initialize peek to false
		peek = false;

		// turnLeft to face a wall.
		// getHeading() % 90 means the remainder of
		// getHeading() divided by 90.
		turnLeft(getHeading() % 90);
		ahead(moveAmount);
		// Turn the gun to turn right 90 degrees.
		peek = true;
		turnGunRight(90);
		turnRight(90);

	
		
		while (true) {
		
			
			if (teammate >= 2) {
				deffensive = false;
				nr++;
			}
			if(nr==1){
				
				ahead(300);
			}
			
			if (deffensive) {
				move1.update(moveAmount, right);
			} else {
				move2.update();
			}

		}
	}

public void onScannedRobot(ScannedRobotEvent e) {

		if (isTeammate(e.getName())) {
			return;
		} else if (!deffensive) {
			fire(3);

		}
		// Calculate enemy bearing
		double enemyBearing = this.getHeading() + e.getBearing();
		// Calculate enemy's position
		double enemyX = getX() + e.getDistance() * Math.sin(Math.toRadians(enemyBearing));
		double enemyY = getY() + e.getDistance() * Math.cos(Math.toRadians(enemyBearing));

		try {
			// Send enemy position to teammates
			Point point = new Point();
			point.setLocation(enemyX, enemyY);
			broadcastMessage(point);
		} catch (IOException ex) {
			out.println("Unable to send order: ");
			ex.printStackTrace(out);
		}

	}

	// Don't get too close to the walls

public boolean test() {
		return (
		// we're too close to the left wall
		(getX() <= wallMargin ||
		// or we're too close to the right wall
				getX() >= getBattleFieldWidth() - wallMargin ||
				// or we're too close to the bottom wall
				getY() <= wallMargin ||
				// or we're too close to the top wall
				getY() >= getBattleFieldHeight() - wallMargin));
	}
	public void onRobotDeath(RobotDeathEvent event) {
		if (isTeammate(event.getName())) {
			teammate++;
		} else {
			enemy++;
		}
	}

	/**
	 * Skriv en metod som kollar när roboten åker in i en vägg
	 */
public void onHitRobot(HitRobotEvent e) {
		// If he's in front of us, set back up a bit.
		if (e.getBearing() > -90 && e.getBearing() < 90) {
			turnRight(180);
			turnGunRight(180);
			ahead(moveAmount);
			right =!right;
		} // else he's in back of us, so set ahead a bit.
		else {
			ahead(100);
		}
	}


/**
 * Method that returns the angle to a coordinate (x,y) from our robot.
 * 
 * @param x
 *            is the x coordinate.
 * @param y
 *            is the y coordinate.
 * @return the angle to the coordinate (x,y).
 */
private double angleTo(double x, double y) {
	return Math.atan2(x - getX(), y - getY());
}

/**
 * Method that returns the bearing to a coordinate (x,y) from the position and heading of our
 * robot. The bearing is the delta angle between the heading of our robot and the angle of the
 * specified coordinate.
 * 
 * @param x
 *            is the x coordinate.
 * @param y
 *            is the y coordinate.
 * @return the angle to the coordinate (x,y).
 */
private double bearingTo(double heading, double x, double y) {
	return Utils.normalRelativeAngle(angleTo(x, y) - heading);
}
	/**
	 * onScannedRobot: What to do when you see another robot
	 */
	/*
	 * public void onScannedRobot(ScannedRobotEvent e) { // Replace the next
	 * line with any behavior you would like fire(1); }
	 */

	/**
	 * onHitByBullet: What to do when you're hit by a bullet
	 */
	/*
	 * public void onHitByBullet(HitByBulletEvent e) { // Replace the next line
	 * with any behavior you would like back(10); }
	 */

	/**
	 * onHitWall: What to do when you hit a wall
	 */

	/*
	 * public void onHitRobot(HitRobotEvent e) { // If he's in front of us, set
	 * back up a bit. if (e.getBearing() > -90 && e.getBearing() < 90) {
	 * back(100); //turnRight(180); //right=!right; } // else he's in back of
	 * us, so set ahead a bit. else { ahead(100); } }
	 */
	/*
	 * public void onHitWall(HitWallEvent e) { // Replace the next line with any
	 * behavior you would like back(20); }
	 */

}
