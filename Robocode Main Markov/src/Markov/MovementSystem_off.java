package Grupp03;

public class MovementSystem_off {
private Robot05 robot;
	public MovementSystem_off(Robot05 robot){
		
		
		this.robot=robot;
		
	}
	public void update(){
		
		// Tell the game that when we take move,
		// we'll also want to turn right... a lot.
		robot.setTurnRight(10000);
		// Limit our speed to 5
		robot.setMaxVelocity(5);
		// Start moving (and turning)
		robot.ahead(10000);
		// Repeat.
	}
	
	
}
